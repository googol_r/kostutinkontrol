import mqtt from 'mqtt'
import { EventEmitter } from 'node:events'

const statusTopic = 'kostutinkontrol/status'
const humidityTopic = 'airgradient/makuuhuone/sensor/humidity/state'
const powerTopic = 'zigbee2mqtt/virta/ilmankostutin'
const powerSetTopic = 'zigbee2mqtt/virta/ilmankostutin/set'

async function main() {
    const desiredHumidity = Number.parseInt(process.env.DESIRED_HUMIDITY, 10)
    const turnOnAt = Number.parseInt(process.env.TURN_ON_AT, 10)
    const turnOffAt = Number.parseInt(process.env.TURN_OFF_AT, 10)
    const windowSensorNames = (process.env.WINDOW_SENSORS ?? "").split(',')
    const windowSensorTopicMapping = new Map(windowSensorNames.map((name) => [`zigbee2mqtt/ikkuna/${name}`, name]))

    const terminationSignal = abortOnTerminationSignal(['SIGINT', 'SIGTERM'])

    const mqttClient = mqtt.connect({
        port: process.env.MQTT_PORT,
        host: process.env.MQTT_HOST,
        path: process.env.MQTT_PATH,
        username: process.env.MQTT_USERNAME,
        password: process.env.MQTT_PASSWORD,
        protocol: process.env.MQTT_PROTOCOL,
        will: {
            topic: statusTopic,
            payload: Buffer.from("offline"),
            retain: true,
        },
    })

    const humidityControl = new HumidityControl(desiredHumidity, turnOnAt, turnOffAt, windowSensorNames)
    const tankEmptyNotifier = new TankEmptyNotifier()
    const haClient = new HomeAssistantClient(process.env.HOME_ASSISTANT_ADDRESS, process.env.HOME_ASSISTANT_TOKEN)

    terminationSignal.addEventListener('abort', () => {
        console.log('shutting down')
        mqttClient.end()
        humidityControl.removeAllListeners()
        tankEmptyNotifier.removeAllListeners()
    })

    mqttClient.on('connect', () => {
        console.log('Connected to mqtt server')
        mqttClient.publish(statusTopic, Buffer.from('online'), { retain: true })
        for (const topic of [humidityTopic, powerTopic, ...windowSensorTopicMapping.keys()]) {
            mqttClient.subscribe(topic, (err) => {
                if (err) {
                    console.error(`Failed to subscribe to topic ${topic}`, err)
                } else {
                    console.log(`Subscribed to topic ${topic}`) 
                }
            })
        }
    })

    mqttClient.on('message', (topic, message) => {
        switch (topic) {
            case humidityTopic: {
                const humidity = Number(message.toString('utf8'))
                console.log('Humidity update', { humidity })
                humidityControl.setHumidity(humidity)
                break
            }
            case powerTopic: {
                const parsedMessage = JSON.parse(message.toString('utf8'))
                const switchState = parsedMessage.state === 'ON'
                const power = parsedMessage.power
                console.log("Power switch update", { switchState, power })
                humidityControl.setPowerState(switchState)
                tankEmptyNotifier.setPowerState(switchState, power)
                break
            }
            default: {
                if (windowSensorTopicMapping.has(topic)) {
                    const windowName = windowSensorTopicMapping.get(topic)
                    const parsedMessage = JSON.parse(message.toString('utf8'))
                    const contact = parsedMessage.contact ?? false
                    console.log("Window contact change", { windowName, contact })
                    humidityControl.setWindowOpen(windowName, !contact)
                } else {
                    console.warn(`Message received on unexpected topic ${topic}`)
                }
                break
            }
        }
    })

    humidityControl.startTimer(terminationSignal)
    humidityControl.on('stateChange', (desiredState) => {
        const outputState = desiredState ? "ON" : "OFF"
        console.log('Setting switch to state:', outputState)
        mqttClient.publish(powerSetTopic, Buffer.from(JSON.stringify({ state: outputState })))
    })

    tankEmptyNotifier.on('deviceStopped', () => {
        console.log('Humidifier has stopped')
        haClient.sendNotification("Ilmankostutin on kytketty pois päältä tai säiliö on tyhjentynyt.", "Ilmankostutin vaatii apua").catch((error) => {
            console.error('Error while sending notification to home assistant', { error })
        })
    })
}

function abortOnTerminationSignal(
  signals,
) {
  const controller = new AbortController()

  for (const signal of signals) {
    process.on(signal, () => controller.abort(signal))
  }

  return controller.signal
}

class HumidityControl extends EventEmitter {
    constructor(desiredHumidity, turnOnAt, turnOffAt, windowNames) {
        super()

        console.log('Humidity control config.', { desiredHumidity, turnOnAt, turnOffAt, windowNames })

        this.desiredHumidity = desiredHumidity
        this.turnOnAt = turnOnAt
        this.turnOffAt = turnOffAt
        this.isWindowOpen = new Map(windowNames.map((name) => [name, false]))
        this.isAnyWindowOpen = false
        this.currentHumidity = null
        this.currentState = false
        this.desiredState = false
    }

    startTimer(abortSignal) {
        const timerId = setInterval(() => {
            this.updateState()
        }, 5 * 60 * 1000)

        abortSignal.addEventListener('abort', () => clearInterval(timerId))
    }

    setHumidity(humidity) {
        this.currentHumidity = humidity
        this.updateState()
    }

    setPowerState(state) {
        this.currentState = state
        this.updateState()
    }

    setWindowOpen(windowName, isOpen) {
        this.isWindowOpen.set(windowName, isOpen)
        this.isAnyWindowOpen = this.checkIsAnyWindowOpen()
        this.updateState()
    }

    checkIsAnyWindowOpen() {
        for (const isOpen of this.isWindowOpen.values()) {
            if (isOpen) {
                return true
            }
        }
        return false
    }

    updateState() {
        const currentTime = new Date()
        const currentHours = currentTime.getHours()

        if (this.isAnyWindowOpen || currentHours < this.turnOnAt || currentHours >= this.turnOffAt) {
            this.setDesiredState(false)
            return
        }

        this.setDesiredState(this.currentHumidity < this.desiredHumidity)
    }

    setDesiredState(desiredState) {
        this.desiredState = desiredState

        if (this.currentState != desiredState) {
            this.emit('stateChange', desiredState)
        }
    }
}

class TankEmptyNotifier extends EventEmitter {
    constructor() {
        super()

        this.switchState = false
        this.currentPower = 0
        this.switchTurnedOnAt = null
        this.waitTimeBeforeNotification = 1000 * 30
        this.notificationSent = false
    }

    setPowerState(switchState, currentPower) {
        const currentTimestamp = new Date()
        if (switchState && !this.switchState) {
            this.switchTurnedOnAt = currentTimestamp
            this.notificationSent = false
        } else if (!switchState) {
            this.switchTurnedOnAt = null
        }

        this.switchState = switchState
        this.currentPower = currentPower

        if (this.switchState && this.currentPower < 10 && !this.notificationSent) {
            const timeSinceTurnedOn = currentTimestamp - this.switchTurnedOnAt
            if (timeSinceTurnedOn > this.waitTimeBeforeNotification) {
                this.notificationSent = true
                this.emit('deviceStopped')
            }
        }
    }
}

class HomeAssistantClient {
    constructor(address, token) {
        console.log("HA client config", { address })
        this.address = new URL(address)
        this.token = token
    }

    async sendNotification(message, title) {
      await retry(async () => {
          const url = new URL("api/services/notify/notify", this.address)
          const data = JSON.stringify({
              message,
              title,
          })

          const response = await fetch(url, {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${this.token}`,
              "Content-Type": "application-json",
              Accept: "application/json",
            },
            body: data,
          })

          console.log('Received response from HA:', response.statusCode)
      })
    }
}

async function retry(func, config = {}) {
  const {
    times = 5,
    delayMs = 3000,
  } = config

  const errors = []

  for (let tries = 0; tries < times; tries++) {
    try {
      return await func()
    } catch (error) {
      console.log(`Error in retry helper. Retrying after ${delayMs}ms.`, { error: lastError })
      errors.push(error)
    }

    await delay(delayMs)
  }

  throw new RetryAggregateError(errors)
}

function delay(timeMs) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeMs)
  })
}

class RetryAggregateError extends Error {
  constructor(errors) {
    super("The operation failed with retries, these are the errors that happened.")
    this.errors = errors
  }

  toJson() {
    return {
      message: this.message,
      errors: this.errors,
    }
  }
}

await main()
